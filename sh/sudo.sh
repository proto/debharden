localedef -i en_US -f UTF-8 en_US.UTF-8

apt-get update && apt-get upgrade

apt install -y gnupg2

wget -q https://deb.parrot.sh/parrot/keyring.gpg -O- | apt-key add -

> /etc/apt/sources.list

cat <<EOT >> /etc/apt/sources.list
## stable repository parrotos
deb https://ftp.osuosl.org/pub/parrotos lts main contrib non-free
deb https://ftp.osuosl.org/pub/parrotos lts-security main contrib non-free
#deb-src https://ftp.osuosl.org/pub/parrotos lts main contrib non-free
#deb-src https://ftp.osuosl.org/pub/parrotos lts-security main contrib non-free
EOT

apt-get update -y && apt-get upgrade -y

localedef -i en_US -f UTF-8 en_US.UTF-8

apt-get install -y curl sudo htop

apt-get install -y git



git clone https://codeberg.org/proto/debharden

cd debharden


chmod +x lockdown.sh

./lockdown.sh



#chmod +x debian_hardening.sh
#./debian_hardening.sh


# Needs to be run there

echo "!! Remove bluetooth systemd services"

sudo rm /etc/systemd/system/dbus-org.bluez.service
sudo rm /etc/systemd/user/dbus-org.bluez.obex.service
sudo rm -rf /etc/systemd/system/bluetooth.target.wants
sudo apt remove blueman
sudo rm /etc/systemd/system/multi-user.target.wants/blueman-mechanism.service
sudo mv /usr/share/dbus-1/services/org.blueman.Applet.service /usr/share/dbus-1/services/org.blueman.Applet.bak




echo "!! Systemd/system.conf and Systemd/user.conf"
SYSTEMCONF='/etc/systemd/system.conf'
USERCONF='/etc/systemd/user.conf'
sed -i 's/^#DumpCore=.*/DumpCore=no/' "$SYSTEMCONF"
sed -i 's/^#CrashShell=.*/CrashShell=no/' "$SYSTEMCONF"
sed -i 's/^#DefaultLimitCORE=.*/DefaultLimitCORE=0/' "$SYSTEMCONF"
sed -i 's/^#DefaultLimitNOFILE=.*/DefaultLimitNOFILE=1024/' "$SYSTEMCONF"
sed -i 's/^#DefaultLimitNPROC=.*/DefaultLimitNPROC=1024/' "$SYSTEMCONF"

sed -i 's/^#DefaultLimitCORE=.*/DefaultLimitCORE=0/' "$USERCONF"
sed -i 's/^#DefaultLimitNOFILE=.*/DefaultLimitNOFILE=1024/' "$USERCONF"
sed -i 's/^#DefaultLimitNPROC=.*/DefaultLimitNPROC=1024/' "$USERCONF"





echo "!! sudo configuration"

if ! grep -qER '^Defaults.*use_pty$' /etc/sudo*; then
  echo "Defaults use_pty" > /etc/sudoers.d/011_use_pty
fi

if ! grep -qER '^Defaults.*logfile' /etc/sudo*; then
  echo 'Defaults logfile="/var/log/sudo.log"' > /etc/sudoers.d/012_logfile
fi

if ! grep -qER '^Defaults.*pwfeedback' /etc/sudo*; then
  echo 'Defaults !pwfeedback' > /etc/sudoers.d/013_pwfeedback
fi

if ! grep -qER '^Defaults.*visiblepw' /etc/sudo*; then
  echo 'Defaults !visiblepw' > /etc/sudoers.d/014_visiblepw
fi

if ! grep -qER '^Defaults.*passwd_timeout' /etc/sudo*; then
  echo 'Defaults passwd_timeout=1' > /etc/sudoers.d/015_passwdtimeout
fi

if ! grep -qER '^Defaults.*timestamp_timeout' /etc/sudo*; then
  echo 'Defaults timestamp_timeout=5' > /etc/sudoers.d/016_timestamptimeout
fi

find /etc/sudoers.d/ -type f -name '[0-9]*' -exec chmod 0440 {} \;

if ! grep -qER '^auth required pam_wheel.so' /etc/pam.d/su; then
  echo "auth required pam_wheel.so use_uid group=sudo" >> /etc/pam.d/su
fi

if [[ $VERBOSE == "Y" ]]; then
  sudo -ll
  echo
fi

echo "!! umask"

if [ -f /etc/init.d/rc ]; then
  sed -i 's/umask 022/umask 077/g' /etc/init.d/rc
fi

if ! grep -q -i "umask" "/etc/profile" 2> /dev/null; then
  echo "umask 077" >> /etc/profile
fi

if ! grep -q -i "umask" "/etc/bash.bashrc" 2> /dev/null; then
  echo "umask 077" >> /etc/bash.bashrc
fi

if ! grep -q -i "TMOUT" "/etc/profile.d/*" 2> /dev/null; then
  echo -e 'TMOUT=600\nreadonly TMOUT\nexport TMOUT' > '/etc/profile.d/autologout.sh'
  chmod +x /etc/profile.d/autologout.sh
fi